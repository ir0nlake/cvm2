
## CollabVM Server 2.0
The second version of the CollabVM Server, now with less Half-Life 3.
## Building

```
mkdir build
cd build
cmake .. # Generates the CMake build files.
(Linux with default generator) make -j$(nproc) # Uses GNU make to build the server.
(Windows with default generator) cmake --build . -j %NUMBER_OF_PROCESSORS% # Uses MSBuild to build the server.
```

Currently only being mainly tested on Windows and Linux.

## CI Build Status

| Build OS (CI Platform)  | Status  |
|---|---|
| Linux (Ubuntu 18.04) (Gitlab)  | [![pipeline status](https://gitlab.com/ir0nlake/cvm2/badges/master/pipeline.svg)](https://gitlab.com/ir0nlake/cvm2/commits/master)  |
| Windows (Visual Studio 2017) (AppVeyor)  | [![Build status](https://ci.appveyor.com/api/projects/status/a2typdhbtsjo1i99?svg=true)](https://ci.appveyor.com/project/ir0nlake28745/cvm2)  |
