
namespace collabvm::virt {
	
	class virtcontroller {
		// base virtualizer interface
	public:
		virtual void start() {}
		virtual void stop() {}
		virtual void reset() {} // restore snapshot then .start()
		bool active;
	};
	
}
