#if defined(_MSC_VER)
	// hacky but works
	#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#endif
#include "ws_server.h"
#include <iostream>
#include <stdexcept>
using std::cout;


void fail(boost::system::error_code ec, char const* what) {
    std::cerr << "WebSocket server error: " << what << ": " << ec.message() << "\n";
}

// right now this shitty piece of shit just echos
// later it will interconnect with the internal "server"

class session : public std::enable_shared_from_this<session> {
	websocket::stream<tcp::socket> ws_;
	boost::asio::strand<boost::asio::io_context::executor_type> strand_;
	boost::beast::multi_buffer buffer_;

public:
    // Take ownership of the socket
    explicit session(tcp::socket socket)
		: ws_(std::move(socket))
		, strand_(ws_.get_executor()) {}

    // Start the asynchronous operation
	void run() {
        // Accept the websocket handshake
		ws_.async_accept(
			boost::asio::bind_executor(
				strand_,
				std::bind(
				&session::on_accept,
				shared_from_this(),
				std::placeholders::_1)));
	}

	void on_accept(boost::system::error_code ec) {
		if(ec)
			return fail(ec, "accept");

		do_read();
	}

	void do_read() {
		// Read a message into our buffer
		ws_.async_read(
			buffer_,
			boost::asio::bind_executor(
				strand_,
				std::bind(
					&session::on_read,
					shared_from_this(),
					std::placeholders::_1,
					std::placeholders::_2)));
	}

	void on_read(boost::system::error_code ec, std::size_t bytes_transferred){
		boost::ignore_unused(bytes_transferred);

		if(ec == websocket::error::closed)
			return;

		if(ec)
			fail(ec, "read");
		
		ws_.text(ws_.got_text());
		ws_.async_write(
			buffer_.data(),
			boost::asio::bind_executor(
				strand_,
				std::bind(
					&session::on_write,
					shared_from_this(),
					std::placeholders::_1,
					std::placeholders::_2)));
	}

	void on_write(boost::system::error_code ec, std::size_t bytes_transferred){
		boost::ignore_unused(bytes_transferred);

		if(ec)
			return fail(ec, "write");
		
		// Clear the buffer
		buffer_.consume(buffer_.size());
		
		// Do another read
		do_read();
    }
	
};

class listener : public std::enable_shared_from_this<listener> {
	tcp::acceptor acceptor_;
	tcp::socket socket_;
public:

	listener(boost::asio::io_context& ioc, tcp::endpoint endpoint) : acceptor_(ioc), socket_(ioc) {
		boost::system::error_code ec;
		
		// Open the acceptor
		acceptor_.open(endpoint.protocol(), ec);
		if(ec){
			fail(ec, "open");
			return;
		}

		// Allow address reuse
		acceptor_.set_option(boost::asio::socket_base::reuse_address(true), ec);
		if(ec){
			fail(ec, "set_option");
			return;
		}

        // Bind to the server address
		acceptor_.bind(endpoint, ec);
		if(ec){
			fail(ec, "bind");
			return;
		}

		// Start listening for connections
		acceptor_.listen(boost::asio::socket_base::max_listen_connections, ec);
		if(ec){
			fail(ec, "listen");
			return;
		}
		
    }

	// Start accepting incoming connections
	void run(){
		if(!acceptor_.is_open())
			return;
		do_accept();
    }

	void do_accept() {
		acceptor_.async_accept(
			socket_,
			std::bind(
				&listener::on_accept,
				shared_from_this(),
				std::placeholders::_1));
	}

    void on_accept(boost::system::error_code ec) {
		if(ec){
			fail(ec, "accept");
		} else {
			cout << "Accepting connection from somewhere\n";
			// Create the session and run it
			std::make_shared<session>(std::move(socket_))->run();
		}

		// Accept another connection
		do_accept();
    }
	
};

namespace collabvm::ws_server {
	constexpr int thread_count = 2;
	bool started = false;
	boost::asio::io_context ioc{thread_count};
	std::vector<std::thread> threads;
	
	void start(unsigned short port) {
		// TODO: map ws (map to collabvm::server::*)
		started = true;
		std::make_shared<listener>(ioc, tcp::endpoint{boost::asio::ip::make_address("0.0.0.0"), port})->run();
		threads.reserve(thread_count - 1);
		for(auto i = thread_count - 1; i > 0; --i) {
			threads.emplace_back([]{
				ioc.run();
			});
		}
		ioc.run();
	}
	
	void stop(){
		if(!started){
			throw std::runtime_error("cannot stop websocket server when not started");
		}
		
		ioc.stop(); // Stop ASIO I/O context, causing all listeners to die when they get the notice
	}
	
}