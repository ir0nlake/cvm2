#include <string>
#include <vector>

namespace collabvm::datamodels {
	// all data structures used by the server routines
	
	enum user_role {
		guest, // #<guestname>
		user, // #<username>
		chatmod, // can only moderate chat, shown as ^USERNAME
		admin // is shown as @USERNAME in chat
	};
	
	struct user {
		std::string username;
		// TODO: add argon2 passwd field
		bool in_turn;
		user_role role;
	};
	
	struct vm_chat_channel {
		std::string name; // set by channel creator, cannot be changed
		std::string description; // set by mod
		vector<string> messages;
		user creator; // creator of the channel
		vector<user> participants; // all users in channel
		vector<user> chanops; // all operators of channel (includes creator)
	};
}