#include <iostream>
#include <boost/program_options.hpp>
#include <boost/asio/version.hpp>
#include <config.h>
#include <signal.h>
#include <stdexcept>
#include "ws_server.h"
using std::cout;
namespace po = boost::program_options;

constexpr unsigned short default_port = 6004;
unsigned short server_port = default_port;
po::options_description opt_desc("CollabVM Server command-line options");

void signal_handler(int signal){
	if(signal == SIGTERM){
		try {
			collabvm::ws_server::stop();
		} catch (std::runtime_error e){
			cout << e.what() << std::endl;
		}
	}
}

int main(int argc, char** argv){
	opt_desc.add_options()
	("help", "Display this message.")
	("version", "Display CollabVM Server version.")
	("port", po::value<unsigned short>(), "Set port from range of 1024-65535.");
	
	po::variables_map varmap;
	try {
		po::store(po::parse_command_line(argc, argv, opt_desc), varmap);
		po::notify(varmap);

		if (varmap.count("help")) {
			cout << opt_desc << "\n";
			return 0;
		}

		if (varmap.count("version")) {
			cout << "CollabVM Server version " << CVM_VERSIONMAJ << "." << CVM_VERSIONMIN << ".\n" << 
			"(C) 2018 ir0nlake, under the GNU Affero General Public License version 3.\n" <<
			"Library versions:\n" <<
			"Boost: " << BOOST_VERSION / 100000 << '.' << BOOST_VERSION / 100 % 1000 << '.' << BOOST_VERSION % 100 << "\n"
			"Boost.Beast: " << BOOST_BEAST_VERSION << "\n"
			"Boost.Asio: " << BOOST_ASIO_VERSION / 100000 << '.' << BOOST_ASIO_VERSION / 100 % 1000 << '.' << BOOST_ASIO_VERSION % 100 << "\n";
			return 0;
		}

		if (varmap.count("port")) {
			if (varmap["port"].as<unsigned short>() < 1024 || varmap["port"].as<unsigned short>() > 65535) {
				std::cerr << "Invalid port " << varmap["port"].as<unsigned short>() << " specified.\n";
				return 1;
			}
			server_port = varmap["port"].as<unsigned short>();
		}	
	} catch (po::unknown_option& e) {
		std::cerr << "Unknown option '" << e.get_option_name() << "'." << std::endl;
		return 1;
	} catch (po::error& e) {
	    std::cerr << e.what() << std::endl;
	    return 1;
	}
	cout << "Starting CollabVM Server on port :" << server_port << "...\n";
	// TODO: safe shutdown (call collabvm::server::stop() or something like that then call stop() on ws_server)
	signal(SIGTERM, signal_handler);
	collabvm::ws_server::start(server_port);
}